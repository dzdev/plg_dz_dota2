<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.joomla
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class PlgDZDota2 extends JPlugin
{
    function __construct(&$subject, $config = array()) {
        parent::__construct($subject, $config = array());
        
        // Add include paths for dota2 models and tables
        JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_dota2/models', 'Dota2Model');
        JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_dota2/tables');
    }
    
	public function onTextPrepare($context, &$text)
	{
        $this->_replaceHero($text);
        $this->_replaceItem($text);
        $this->_replaceAbility($text);
        
        return true;
	}
	
	private function _load($model, $name)
	{
        $model = JModelLegacy::getInstance($model, 'Dota2Model');
        
        return $model->getData(null, $name);
	}
	
	private function _replaceHero(&$text)
	{
        // Simple check to determine whether bot should continue
        if (strpos($text, 'loadhero') === false)
            return;
            
        // Regex for load hero
        $regexhero = '/{loadhero.*?}/i';
        
        // Find hero matches
        preg_match_all($regexhero, $text, $matches, PREG_SET_ORDER);
        // No matches, skip this
        if ($matches)
        {
            foreach ($matches as $match) {
                $shortcode = $match[0];
                $item = $this->_load('Hero', self::_getMainAttribute($shortcode));
                
                $width = self::_getAttribute($shortcode, 'width');
                $height = self::_getAttribute($shortcode, 'height');
                
                $output  =  '<a href="' . $item->link . '" class="dota2-image dota2-hero" title="' . $item->dname . '">';
                $output .=      '<img src="' . $item->image['dota2']['hphover'] . '" alt="' . $item->dname . '" ' . ($width ? " width='$width' " : '') . ($height ? " height='$height' " : '') .'/>';
                $output .=  '</a>';
                // We should replace only first occurrence
                $text = preg_replace("|$shortcode|", addcslashes($output, '\\$'), $text, 1);
            }
        }
	}
	
	private function _replaceItem(&$text)
	{
        // Simple check to determine whether bot should continue
        if (strpos($text, 'loaditem') === false)
            return;
        
        // Regex for loaditem
        $regexitem = '/{loaditem.*?}/i';
        
        // Find item matches
        preg_match_all($regexitem, $text, $matches, PREG_SET_ORDER);
        // No matches, skip this
        if ($matches)
        {
            foreach ($matches as $match) {
                $shortcode = $match[0];
                $item = $this->_load('Item', self::_getMainAttribute($shortcode));

                $width = self::_getAttribute($shortcode, 'width');
                $height = self::_getAttribute($shortcode, 'height');
                
                $output  =  '<a href="' . $item->link . '" class="dota2-image dota2-item" title="' . $item->dname . '">';
                $output .=      '<img src="' . $item->image['dota2']['lg'] . '" alt="' . $item->dname . '" ' . ($width ? " width='$width' " : '') . ($height ? " height='$height' " : '') .'/>';
                $output .=  '</a>';
                // We should replace only first occurrence
                $text = preg_replace("|$shortcode|", addcslashes($output, '\\$'), $text, 1);
            }
        }
	}
	
	private function _replaceAbility(&$text)
	{
        // Simple check to determine whether bot should continue
        if (strpos($text, 'loadability') === false)
            return;
        
        // Regex for loadability
        $regexability = '/{loadability.*?}/i';
        
        // Find ability matches
        preg_match_all($regexability, $text, $matches, PREG_SET_ORDER);
        // No matches, skip this
        if ($matches)
        {
            foreach ($matches as $match) {
                $shortcode = $match[0];
                $item = $this->_load('Ability', self::_getMainAttribute($shortcode));
                
                $width = self::_getAttribute($shortcode, 'width');
                $height = self::_getAttribute($shortcode, 'height');
                
                $output = '<a href="' . $item->hero_link . '" class="dota2-image dota2-ability" title="' . $item->dname . '">';
                $output .=      '<img src="' . $item->image['dota2']['hp1'] . '" alt="' . $item->dname . '" title="'. $item->dname .'" ' . ($width ? " width='$width' " : '') . ($height ? " height='$height' " : '') .'/>';
                $output .=  '</a>';
                // We should replace only first occurrence
                $text = preg_replace("|$shortcode|", addcslashes($output, '\\$'), $text, 1);
            }
        }
    }

    private static function _getAttribute($shortcode, $attrib) {
        $regex = "/" . $attrib . "\s*=\s*[\"']?([A-Za-z0-9]+)[\"']?/i";
        
        // Find attribute in the shortcode
        preg_match($regex, $shortcode, $matches);
        if ($matches) {
            return $matches[1];
        } else {
            return null;
        }
    }
    
    private static function _getMainAttribute($shortcode) {
        $regex = "/{.*\s+(\w+)}/";
        preg_match($regex, $shortcode, $matches);
        if ($matches) {
            return $matches[1];
        } else {
            return null;
        }
    }
}
